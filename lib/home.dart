import 'dart:ffi';
import 'package:flutter/material.dart';
import 'package:masidclothing/signup.dart';
import 'package:masidclothing/splashscreen.dart';
import 'dart:async';
import 'home.dart';



class homepage extends StatelessWidget {
  const homepage({super.key});



@override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          margin: EdgeInsets.all(24),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _header(context),
              _inputField(context),
              _forgotPassword(context),
              _signup(context),
            ],
          ),
        ),
      ),
    );
  }

  _header(context) {
    return Column(
      children: [
        Text(
          "MASID",
          style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
        ),
        Text("login"),
      ],
    );
  }

  _inputField(context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        TextField(
          decoration: InputDecoration(
              hintText: "Email or Number",
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5),
                  borderSide: BorderSide.none),
              fillColor: Theme.of(context).primaryColor.withOpacity(0.1),
              filled: true,
              prefixIcon: Icon(Icons.person)),
        ),
        SizedBox(height: 10),
        TextField(
          decoration: InputDecoration(
            hintText: "Password",
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(5),
                borderSide: BorderSide.none),
            fillColor: Theme.of(context).primaryColor.withOpacity(0.1),
            filled: true,
            prefixIcon: Icon(Icons.lock_outline),
          ),
          obscureText: true,
        ),
        SizedBox(height: 30),



        
        ElevatedButton(
          onPressed: () {_goToNextPage(context);
          },
          child: Text(
            "Login",
            style: TextStyle(fontSize: 20),
          ),
          style: ElevatedButton.styleFrom(
            primary: Colors.black,
            shape: StadiumBorder(),
            padding: EdgeInsets.symmetric(vertical: 16),
          ),
        )
      ],
    );
  }

void _goToNextPage(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => splashscreen()),
    );
  }
}





  _forgotPassword(context) {
    return TextButton(onPressed: () {
      _goToForgotPassword(context);
    }, child: Text("Forgot password?"));
  }

  void _goToForgotPassword(BuildContext context){
    Navigator.push(context,
    MaterialPageRoute(builder: (context) => signup() )
    );
  }

  _signup(context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text("Dont have an account? "),
        TextButton(onPressed: () {
          _goToSignup(context);
        }, child: Text("Sign Up"))
      ],
    );
  }

    void _goToSignup(BuildContext context){
    Navigator.push(context,
    MaterialPageRoute(builder: (context) => signup() )
    );
  }

