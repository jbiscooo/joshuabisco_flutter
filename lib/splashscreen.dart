import 'dart:ffi';
import 'package:flutter/material.dart';
import 'dart:async';
import 'home.dart';

class splashscreen extends StatefulWidget{

  @override
  _splashscreenState createState() => _splashscreenState();

}

class _splashscreenState extends State<splashscreen>{

  @override
  void initState(){
    super.initState();

    Timer(Duration(seconds: 3), (){
      Navigator.pushReplacement(context, 
      MaterialPageRoute(builder: (context) => homepage()));
    });
  }



  @override
  Widget build(BuildContext context){
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 0, 0, 0),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("MASID" , style: TextStyle(fontSize: 45,color: Colors.white, fontWeight: FontWeight.bold, letterSpacing:1, 
            ),
            ),
            SizedBox(height: 30),
            Text(
              "Masid Clothing",
              style: TextStyle(
                fontSize: 18,
              color: Colors.white,
             fontStyle: FontStyle.italic,
             
            ),
            ),
            SizedBox(height: 10.0),
          CircularProgressIndicator(
            
            backgroundColor: Colors.grey[300],
          ),
          ],
          
        ),
      )
    );
  }
}